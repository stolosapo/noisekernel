# noisekernel

A C++ library with many goodies... some of them:

* __Dependency Injection__: Create Singleton or any other services and inject them in any class or place in your implementation.
* __Argument Parsing__: Parse arguments and use them in any class using dependency injection.
* __Data Modeling__: Create serializable class models that can be easily reflect their own properties.
* __Configuration Files__: Read easily JSON configuration files (or any other JSON file).
* __Executors__: Create parent - child processes and fork processes.
* __Exceptions__: Create and manage domain specific and runtime exceptions.
* __Functional__: Create `Consumers`, `Functions`, `Predicates` and `Suppliers`.
* __GPIO__: Read - Write through GPIO.
* __Interruptions__: Accept and handle easily signals and interruptions.
* __Logging__: Use loggers in any place in your implementation using dependency injection.
* __Menu__: Manage and visualize your implementations through console menus.
* __Observer__: Implement observer pattern in your code and implementations.
* __Serialization__: Serialize into JSON any model.
* __Strategy__: Implement strategy pattern.
* __Async__: Create, handle and synchronize easily threads and thread pools.


### Installation
```
./configure
make
sudo make install
sudo ldconfig /usr/local/lib
```

### Uninstallation
```
sudo make uninstall
```

### Run
```
noisekernel
```

### Use Library
If everything installed fine then pass `-lnoisekernel` parameter in linker, when build your application.
After that and if linker success you should be able to use any of the above features in your code.
