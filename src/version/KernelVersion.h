#ifndef KernelVersion_h__
#define KernelVersion_h__

#include <iostream>

#include "Version.h"

using namespace std;

namespace NoiseKernel
{
    class KernelVersion : public Version
    {
    public:
    	KernelVersion();
    	virtual ~KernelVersion();

    };
}

#endif // KernelVersion_h__
