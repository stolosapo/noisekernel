
#ifndef Console_h_
#define Console_h_

#include <iostream>
#include <string>
#include <limits>

using namespace std;

namespace NoiseKernel
{
    string consoleInLine();

    template <typename T>
    T consoleIn();

    template <typename T>
    void consolePrint(T message);

    template <typename T>
    void consolePrintln(T message);

    void consoleClearScreen();

    /* Implementation */
    template <typename T>
    T consoleIn()
    {
        T message;

    	cin >> message;
    	cin.ignore(numeric_limits<streamsize>::max(), '\n');

    	return message;
    }

    template <typename T>
    void consolePrint(T message)
    {
        cout << message;
    }

    template <typename T>
    void consolePrintln(T message)
    {
        cout << message << endl;
    }
}

#endif /* Console_h_ */
