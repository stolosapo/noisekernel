#include <iostream>
#include <string>

#include "Console.h"

using namespace std;

namespace NoiseKernel
{
    string consoleInLine()
    {
    	string s;

    	getline(cin, s);

    	return s;
    }

    void consoleClearScreen()
    {
        consolePrint<string>(string(50, '\n'));
    }
}
