#ifndef ModelFactory_h__
#define ModelFactory_h__

namespace NoiseKernel
{
    class Model;

    typedef Model* (*ModelFactory)();
}

#endif // ModelFactory_h__
