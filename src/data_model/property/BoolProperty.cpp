#include "BoolProperty.h"

using namespace NoiseKernel;

BoolProperty::BoolProperty(string name)
    : Property(name, BOOL), PropertyValue<bool>()
{

}

BoolProperty::~BoolProperty()
{

}
