#include "DoubleProperty.h"

using namespace NoiseKernel;

DoubleProperty::DoubleProperty(string name)
    : Property(name, DOUBLE), PropertyValue<double>()
{

}

DoubleProperty::~DoubleProperty()
{

}
