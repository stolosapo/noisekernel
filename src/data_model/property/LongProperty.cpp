#include "LongProperty.h"

using namespace NoiseKernel;

LongProperty::LongProperty(string name)
    : Property(name, LONG), PropertyValue<long>()
{

}

LongProperty::~LongProperty()
{

}
