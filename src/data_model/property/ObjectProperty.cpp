#include "ObjectProperty.h"

using namespace NoiseKernel;

ObjectProperty::ObjectProperty(string name, ModelFactory modelFactory)
    : Property(name, OBJECT, modelFactory), PropertyPointerValue<Model>()
{

}

ObjectProperty::~ObjectProperty()
{

}
