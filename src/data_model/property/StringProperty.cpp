#include "StringProperty.h"

using namespace NoiseKernel;

StringProperty::StringProperty(string name)
    : Property(name, STRING), PropertyValue<string>()
{

}

StringProperty::~StringProperty()
{

}
