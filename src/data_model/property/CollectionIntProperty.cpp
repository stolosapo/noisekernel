#include "CollectionIntProperty.h"

using namespace NoiseKernel;

CollectionIntProperty::CollectionIntProperty(string name)
    : Property(name, COLLECTION_INT), PropertyCollectionValue<int>()
{

}

CollectionIntProperty::~CollectionIntProperty()
{

}
