#include "IntProperty.h"

using namespace NoiseKernel;

IntProperty::IntProperty(string name)
    : Property(name, INT), PropertyValue<int>()
{

}

IntProperty::~IntProperty()
{

}
