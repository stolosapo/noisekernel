#ifndef EventArgs_h__
#define EventArgs_h__

namespace NoiseKernel
{
    class EventArgs
    {
    public:
        EventArgs();
        virtual ~EventArgs();
    };
}

#endif // EventArgs_h__
