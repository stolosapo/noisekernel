#ifndef Test_h__
#define Test_h__

#include <iostream>
#include <string>

using namespace std;

namespace NoiseKernel
{
    class Test
    {
    private:
        int value;

    public:
        Test(int value);
        virtual ~Test();

        int getValue();
    };
};

#endif // Test_h__
