#ifndef IService_h__
#define IService_h__

namespace NoiseKernel
{
    class IService
    {
    public:
        IService();
        virtual ~IService();

    };
}

#endif // IService_h__
