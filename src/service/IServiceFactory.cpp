#include "IServiceFactory.h"

using namespace NoiseKernel;

IServiceFactory::IServiceFactory()
{
    this->implementationSelection = "";
}

IServiceFactory::IServiceFactory(string implementationSelection)
{
    this->implementationSelection = implementationSelection;
}

IServiceFactory::~IServiceFactory()
{

}
