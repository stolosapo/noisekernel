#ifndef PrimeFactor_h__
#define PrimeFactor_h__

#include <iostream>
#include <vector>

using namespace std;

namespace NoiseKernel
{
    class PrimeFactor
    {
    public:
        static vector<int> upTo(int upperBound);
        static int getByIndex(int index);
        static int getNextPrime(int prime);
        static bool isPrime(int number);
    };
}

#endif /* PrimeFactor_h__ */
