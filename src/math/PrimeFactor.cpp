#include <iostream>
#include <vector>
#include "PrimeFactor.h"

using namespace std;
using namespace NoiseKernel;

vector<int> PrimeFactor::upTo(int upperBound)
{
    vector<int> result;
    bool isPrime;
    int cnt = 0;

    for (int i = 2; i <= upperBound; i++)
    {
        isPrime = PrimeFactor::isPrime(i);

        if (isPrime)
        {
            result.push_back(i);
        }
    }

    return result;
}

int PrimeFactor::getByIndex(int index)
{
    int cnt = 0;
    int i = 2;
    int primeNumber = 0;
    bool isPrime;

    while(cnt < index)
    {
        isPrime = PrimeFactor::isPrime(i);

        if (isPrime)
        {
            cnt++;
            primeNumber = i;
        }

        i++;
    }

    return primeNumber;
}


int PrimeFactor::getNextPrime(int prime)
{
    prime++;

    if (PrimeFactor::isPrime(prime))
    {
        return prime;
    }

    while (!PrimeFactor::isPrime(prime))
    {
        prime++;
    }

    return prime;
}

/*
**************************
**** Another Function ****
**************************

static bool IsPrime(int i)
{
    if (i == 1) return false; // 1 is not prime
    if (i < 4) return true;   // 2 and 3 are primes
    if ((i >> 1) * 2 == i) return false; // multiples of 2 are not prime
    if (i < 9) return true; // 5 and 7 are primes
    if (i % 3 == 0) return false; // multiples of 3 are not primes

    // If a divisor less than or equal to sqrt(i) is found
    // then i is not prime
    int sqrt = (int)Math.Sqrt(i);
    for (int d = 5; d <= sqrt; d += 6)
    {
        if (i % d == 0) return false;
        if (i % (d + 2) == 0) return false;
    }

    // Otherwise i is prime
    return true;
}

*/

bool PrimeFactor::isPrime(int number)
{
    if (number == 1)
    {
        return false;
    }

    bool isPrime = true;

    for (int j = 2; j <= number; j++)
    {
        if (number % j == 0 && number != j)
        {
            isPrime = false;
            break;
        }
    }

    return isPrime;
}
