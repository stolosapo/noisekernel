#include "LogServiceFactory.h"
#include "LogConsoleService.h"

using namespace NoiseKernel;

LogServiceFactory::LogServiceFactory(): IServiceFactory()
{

}

LogServiceFactory::~LogServiceFactory()
{

}

IService *LogServiceFactory::create()
{
	LogConsoleService* srv = new LogConsoleService;
	return (IService*) srv;
}
