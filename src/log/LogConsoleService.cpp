#include <iostream>
#include <string>
#include "LogConsoleService.h"
#include "../definitions/Colors.h"
#include "../io/Console.h"

using namespace std;
using namespace NoiseKernel;

LogConsoleService::LogConsoleService(): ILogService()
{
	this->useColor = true;
}

LogConsoleService::~LogConsoleService()
{

}

void LogConsoleService::print(string message)
{
	consolePrint<string>(message);
}

void LogConsoleService::printl(string message)
{
	consolePrintln<string>(message);
}

void LogConsoleService::printColor(string COLOR)
{
	if (this->useColor)
	{
		consolePrint<string>(COLOR);
	}
}

void LogConsoleService::trace(string message)
{
	printColor(BOLDGREEN);
	consolePrint<string>(" *** [ TRACE ]: ");

	printColor(WHITE);
	consolePrintln<string>(message);
	printColor(RESET);
}

void LogConsoleService::info(string message)
{
	printColor(BOLDYELLOW);
	consolePrint<string>(" *** [ INFO ]: ");

	printColor(WHITE);
	consolePrintln<string>(message);
	printColor(RESET);
}

void LogConsoleService::debug(string message)
{
	printColor(BOLDBLUE);
	consolePrint<string>(" *** [ DEBUG ]: ");

	printColor(WHITE);
	consolePrintln<string>(message);
	printColor(RESET);
}

void LogConsoleService::warn(string message)
{
	printColor(BOLDMAGENTA);
	consolePrint<string>(" *** [ WARN ]: ");

	printColor(WHITE);
	consolePrintln<string>(message);
	printColor(RESET);
}

void LogConsoleService::error(string message)
{
	printColor(BOLDRED);
	consolePrint<string>(" *** [ ERROR ]: ");

	printColor(WHITE);
	consolePrintln<string>(message);
	printColor(RESET);
}

void LogConsoleService::fatal(string message)
{
	printColor(BOLDRED);
	consolePrint<string>(" *** [ FATAL ]: ");

	printColor(WHITE);
	consolePrintln<string>(message);
	// exit(1);
	printColor(RESET);
}

void LogConsoleService::test()
{
	consolePrint<string>("Hello World!!");
}
