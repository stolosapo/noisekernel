#ifndef ILogService_h__
#define ILogService_h__

#include <iostream>
#include <string>

#include "../service/IService.h"

using namespace std;

namespace NoiseKernel
{
    class ILogService : public IService
    {
    protected:
    	bool useColor;

    public:
    	ILogService(): IService() { };
    	virtual ~ILogService() { };

    	bool getUseColor();
    	void setUseColor(bool useColor);

    	virtual void print(string message) = 0;
    	virtual void printl(string message) = 0;
    	virtual void printColor(string message) = 0;

    	virtual void trace(string message) = 0;
    	virtual void info(string message) = 0;
    	virtual void debug(string message) = 0;
    	virtual void warn(string message) = 0;
    	virtual void error(string message) = 0;
    	virtual void fatal(string message) = 0;

    	virtual void test() = 0;
    };
}

#endif // ILogService_h__
