#ifndef LogConsoleService_h__
#define LogConsoleService_h__

#include <iostream>
#include <string>
#include "ILogService.h"

using namespace std;

namespace NoiseKernel
{
    class LogConsoleService : public ILogService
    {
    public:
    	LogConsoleService();

    	virtual ~LogConsoleService();

    	void print(string message);
    	void printl(string message);
    	void printColor(string COLOR);

    	void trace(string message);
    	void info(string message);
    	void debug(string message);
    	void warn(string message);
    	void error(string message);
    	void fatal(string message);

    	void test();
    };
}

#endif // LogConsoleService_h__
