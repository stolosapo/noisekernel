#ifndef LogLevel_h__
#define LogLevel_h__

namespace NoiseKernel
{
    enum LogLevel
    {

        TRACE = 0,

        DEBUG = 1,

        INFO = 2,

        WARN = 3,

        ERROR = 4,

        FATAL = 5
    };
}

#endif // LogLevel_h__
