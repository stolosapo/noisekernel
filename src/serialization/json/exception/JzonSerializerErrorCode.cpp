#include "JzonSerializerErrorCode.h"

using namespace NoiseKernel;

const DomainErrorCode JzonSerializerErrorCode::JSR0001 = DomainErrorCode("JSR0001", "Could not locate serializer for property '%s'");


JzonSerializerErrorCode::JzonSerializerErrorCode()
{

}

JzonSerializerErrorCode::~JzonSerializerErrorCode()
{

}
