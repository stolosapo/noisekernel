#include "SerializationServiceFactory.h"
#include "json/JzonService.h"

using namespace NoiseKernel;

SerializationServiceFactory::SerializationServiceFactory()
{

}

IService *SerializationServiceFactory::create()
{
	JzonService* srv = new JzonService;
	return  (IService*) srv;
}
