#include <iostream>

#include "Test.h"
#include "version/KernelVersion.h"
#include "task/Thread.h"
#include "task/ThreadInterceptionData.h"
#include "time/TimeServiceFactory.h"
#include "exception/ExceptionMapper.h"
#include "exception/GeneralDomainErrorCode.h"
#include "di/GlobalAppContext.h"
#include "log/LogServiceFactory.h"
#include "io/Console.h"

using namespace std;
using namespace NoiseKernel;

void* testThreadsDelegate(void* data);
void testThreads();
void testExceptions();
void testDi(int argc, char* argv[]);
void registerServices(int argc, char* argv[]);

int main(int argc, char* argv[])
{
    Test t(100);
    KernelVersion ver;
    TimeServiceFactory f;

    ITimeService* timeSrv = (ITimeService*) f.create();

    cout << "NoiseKernel Library" << endl << endl;
    cout << "version: " << ver.version() << endl;
    cout << "This is just a test: " << t.getValue() << endl;
    cout << "Now: " << timeSrv->rawNow() << endl;
    testThreads();
    testExceptions();
    testDi(argc, argv);
    cout << endl;
    cout << "Bye Bye.." << endl;

    consolePrintln<string>("And again..");

    delete timeSrv;
}

void* testThreadsDelegate(void* data)
{
    cout << "** Run in different thread! " << endl;

    return NULL;
}

void testThreads()
{
    Thread th;
    th += testThreadsDelegate;
    th.start(NULL);
    th.wait();

    cout << "testThreads ended" << endl;
}

void testExceptions()
{
    try
    {
        throw RuntimeException("This is a test runtime exception");
    }
    catch (DomainException& e)
    {
        cerr << handle(e) << endl;
    }
    catch (RuntimeException& e)
    {
        cerr << handle(e) << endl;
    }
    catch (exception& e)
    {
        throw e;
    }
}

void testDi(int argc, char* argv[])
{
    /* Initialize AppContext */
	initializeAppContext(&registerServices, argc, argv);

    ILogService* logSrv = inject<ILogService>("logService");

    logSrv->info("This is an injected log test!!");

    /* Clear AppContext */
	deleteAppContext();
}

void registerServices(int argc, char* argv[])
{
	/* Logging Service */
	registerGlobalService<LogServiceFactory>(SINGLETON, "logService", new LogServiceFactory);
}
