#include "GlobalAppContext.h"

#include <map>

#include "InjectionScope.h"

namespace NoiseKernel
{

    AppContext appContext;


    void initializeAppContext(InitializeAppContextDelegate initDelegate, int argc, char* argv[])
    {
    	/* Created context */
    	cout << "* Created appContext: " << &appContext << endl;

    	/* register all services */
    	initDelegate(argc, argv);

    	cout << "* Registered Services" << endl;

    	/* load START_UP services */
    	loadStartupServices();
    }

    void deleteAppContext()
    {

    }

    void loadStartupServices()
    {
    	map<string, InjectionScope> scopes = appContext.getScopes();

    	for (map<string, InjectionScope>::iterator it = scopes.begin();
    		it != scopes.end();
    		++it)
    	{
    		string curServiceName = it->first;
    		InjectionScope curScope = it->second;

    		if (curScope == START_UP)
    		{
    			appContext.getService(curServiceName);
    		}
    	}
    }

}
