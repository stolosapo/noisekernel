#ifndef InitializeAppContextDelegate_h__
#define InitializeAppContextDelegate_h__


namespace NoiseKernel
{
    typedef void (*InitializeAppContextDelegate)(int argc, char* argv[]);
}

#endif // InitializeAppContextDelegate_h__
