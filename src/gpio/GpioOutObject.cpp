#include "GpioOutObject.h"

using namespace NoiseKernel;

GpioOutObject::GpioOutObject() : GpioObject()
{

}

GpioOutObject::GpioOutObject(string gnum) : GpioObject(gnum)
{

}

GpioOutObject::~GpioOutObject()
{

}

int GpioOutObject::setdir_gpio()
{
    return GpioObject::setdir_gpio("out");
}
