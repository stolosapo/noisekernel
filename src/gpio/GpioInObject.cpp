#include "GpioInObject.h"

using namespace NoiseKernel;

GpioInObject::GpioInObject() : GpioObject()
{

}

GpioInObject::GpioInObject(string gnum) : GpioObject(gnum)
{

}

GpioInObject::~GpioInObject()
{

}

int GpioInObject::setdir_gpio()
{
    return GpioObject::setdir_gpio("in");
}
