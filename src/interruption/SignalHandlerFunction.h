#ifndef SignalHandlerFunction_h__
#define SignalHandlerFunction_h__

namespace NoiseKernel
{
    typedef void (*SignalHandlerFunction)(int);
}

#endif // SignalHandlerFunction_h__
